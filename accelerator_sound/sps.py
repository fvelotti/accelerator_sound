from cpymad.madx import Madx
import requests
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

p = 400.0  # beam momentum (GeV/c)
momentum = p  # beam momentum (GeV/c)
Brho = p * 3.3356

N_EX = 10e-6
N_EY = 5e-6
DPP = 1e-4

mad = Madx(stdout=True)

mad.input(
    requests.get(
        "https://gitlab.cern.ch/acc-models/acc-models-sps/-/raw/2021/SPS_LS2_2020-05-26.seq"
    ).text
)
mad.input(
    requests.get(
        "https://gitlab.cern.ch/acc-models/acc-models-sps/-/raw/2021/strengths/ft_q26_extr.str"
    ).text
)
mad.input(
    requests.get(
        "https://gitlab.cern.ch/acc-models/acc-models-sps/-/raw/2021/toolkit/macro.madx"
    ).text
)
mad.input(
    requests.get(
        "https://gitlab.cern.ch/acc-models/acc-models-sps/-/raw/2021/aperture/aperturedb_classes.madx"
    ).text
)

mad.input(
    requests.get(
        "https://gitlab.cern.ch/acc-models/acc-models-sps/-/raw/2021/aperture/aperturedb_elements.madx"
    ).text
)

mad.command.beam(particle="PROTON", pc=p, charge=1)
mad.input("BRHO = BEAM->PC * 3.3356;")
mad.use(sequence="sps")

twiss = mad.twiss(x=1e-3).dframe()

plt.figure()
plt.plot(twiss.s, twiss.dx, label="dx")
plt.show()

interp_d = np.interp(np.linspace(0, 495, 64), twiss.s, twiss.dx)


def normalise(wave: np.ndarray) -> np.ndarray:
    return (wave - wave.min()) / (wave.max() - wave.min()) * 2 - 1


wave1 = np.concatenate([interp_d, -interp_d[::-1]])
wave1[-1] = interp_d[-1]
wave1[0] = interp_d[-1]
wave1 = normalise(wave1)


wave2 = np.interp(np.linspace(1734, 4032, 128), twiss.s, twiss.dx)
wave2[-1] = wave2[0]
wave2 = wave2 - wave2.mean()
wave2 = normalise(wave2)


wave3 = np.interp(np.linspace(0, 428, 128), twiss.s, twiss.x)
wave3[-1] = wave3[0]
wave3 = normalise(wave3)


wave4 = np.interp(np.linspace(0, 2500, 128), twiss.s, twiss.x)
wave4[-1] = wave4[0]
wave4 = normalise(wave4)


twiss2 = mad.twiss(y=10e-3).dframe()

wave5 = np.interp(np.linspace(0, 1030, 128), twiss2.s, twiss2.y)
wave5[-1] = wave5[0]
wave5 = normalise(wave5)


wave6 = np.interp(np.linspace(2300, 3843, 128), twiss2.s, twiss2.y)
wave6[-1] = wave6[0]
wave6 = normalise(wave6)


wave7 = np.interp(np.linspace(0, 128, 128), twiss2.s, twiss2.betx)
wave7[-1] = wave7[0]
wave7 = wave7 - wave7.mean()
wave7 = normalise(wave7)

wave8 = np.interp(
    np.linspace(1250, 1566, 128),
    twiss2.s,
    np.sqrt(twiss.betx + (twiss.dx) ** 2),
)
wave8[-1] = wave8[0]
wave8 = wave8 - wave8.mean()
wave8 = normalise(wave8)


waves = [wave1, wave2, wave3, wave4, wave5, wave6, wave7, wave8]
fig, axes = plt.subplots(2, 4, figsize=(7, 4))
x = np.linspace(0, 1, 128)
for i, (ax, wave) in enumerate(zip(axes.flat, waves)):
    ax.fill_between(x, 0, wave, lw=1, edgecolor="black", facecolor="k")
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_title(f"Wave {i + 1}")
plt.tight_layout()
plt.show()

waves_df = pd.DataFrame(waves).T
waves_df.columns = [f"wave_{i+1}" for i in range(8)]

waves_df.to_csv("waves.csv", index=False)
