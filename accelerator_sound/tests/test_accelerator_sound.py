"""
High-level tests for the  package.

"""

import accelerator_sound


def test_version():
    assert accelerator_sound.__version__ is not None
